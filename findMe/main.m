//
//  main.m
//  findMe
//
//  Created by CyberDesignZ-LT009 on 12/19/12.
//  Copyright (c) 2012 CyberDesignZ-LT009. All rights reserved.
//



// la bla bla bal blabla bla bla

#import <UIKit/UIKit.h>

#import "findMeAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([findMeAppDelegate class]));
    }
}
