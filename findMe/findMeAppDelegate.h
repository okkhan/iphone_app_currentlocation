//
//  findMeAppDelegate.h
//  findMe
//
//  Created by CyberDesignZ-LT009 on 12/19/12.
//  Copyright (c) 2012 CyberDesignZ-LT009. All rights reserved.
//

#import <UIKit/UIKit.h>

@class findMeViewController;

@interface findMeAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) findMeViewController *viewController;

@end
