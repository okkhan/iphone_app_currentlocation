//
//  findMeViewController.m
//  findMe
//
//  Created by CyberDesignZ-LT009 on 12/19/12.
//  Copyright (c) 2012 CyberDesignZ-LT009. All rights reserved.
//

#import "findMeViewController.h"

@interface findMeViewController ()

@end

@implementation findMeViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    locationManager = [[CLLocationManager alloc]init];
    locationManager.delegate = self;
    locationManager.purpose = @"Getting your location";
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager startUpdatingLocation];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*-(void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    //NSLog(@"%g",newLocation.coordinate.latitude);
    //NSLog(@"%g",newLocation.coordinate.longitude);
    NSString *tempStr = [NSString stringWithFormat:@"%g",newLocation.coordinate.latitude];
    [self.CurrentLocLatt setText:tempStr];
    
    tempStr = [NSString stringWithFormat:@"%g",newLocation.coordinate.longitude];
    [self.CurrentLocLong setText:tempStr];
    
    
    tempStr = [NSString stringWithFormat:@"%g",oldLocation.coordinate.longitude];
    [self.OldLocLatt setText:tempStr];

    
    tempStr = [NSString stringWithFormat:@"%g",oldLocation.coordinate.longitude];
    [self.OldLoclong setText:tempStr];
    
}
*/


-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    if ([locations count]>0) {
        CLLocation *anObj = [locations objectAtIndex:0];
        [self getLocationInformationFromLocation:anObj];
    }
}

-(void)getLocationInformationFromLocation:(CLLocation *)location
{
    clGeocoder = [[CLGeocoder alloc]init];
    [clGeocoder reverseGeocodeLocation:location completionHandler:^(NSArray *a,NSError *e)
     {
         CLPlacemark *place = [a objectAtIndex:0];
         NSLog(@"%@", [place.addressDictionary objectForKey:@"Name"]);
         NSLog(@"%@", [place.addressDictionary objectForKey:@"City"]);
         NSLog(@"%@", [place.addressDictionary objectForKey:@"State"]);
         NSLog(@"%@", [place.addressDictionary objectForKey:@"Country"]);
         
         NSString *tempStr = [NSString stringWithFormat:@"%f",location.coordinate.latitude];
         [self.CurrentLocLatt setText:tempStr];
         
         tempStr = [NSString stringWithFormat:@"%f",location.coordinate.longitude];
         [self.CurrentLocLong setText:tempStr];
         
         
         tempStr = [NSString stringWithFormat:@"%@",[place.addressDictionary objectForKey:@"Name"]];
         [self.place setText:tempStr];

         tempStr = [NSString stringWithFormat:@"%@",[place.addressDictionary objectForKey:@"City"]];
         [self.city setText:tempStr];
         
         tempStr = [NSString stringWithFormat:@"%@",[place.addressDictionary objectForKey:@"State"]];
         [self.state setText:tempStr];
         
         tempStr = [NSString stringWithFormat:@"%@",[place.addressDictionary objectForKey:@"Country"]];
         [self.Country setText:tempStr];
     }];
         /*geoLocation.latitude = location.coordinate.latitude;
         geoLocation.longitude = location.coordinate.longitude;
         geoLocation.placeName = [place.addressDictionary objectForKey:@"Name"];
         geoLocation.cityName = [place.addressDictionary objectForKey:@"City"];
         geoLocation.state = [place.addressDictionary objectForKey:@"State"];
         geoLocation.countryName = [place.addressDictionary objectForKey:@"Country"];
          */
    
}


- (void)dealloc {
    [_CurrentLocLatt release];
    [_CurrentLocLong release];
    [_place release];
    [_city release];
    [_state release];
    [_Country release];
    [super dealloc];
}
@end
