//
//  findMeViewController.h
//  findMe
//
//  Created by CyberDesignZ-LT009 on 12/19/12.
//  Copyright (c) 2012 CyberDesignZ-LT009. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>


@interface findMeViewController : UIViewController <CLLocationManagerDelegate>

{
    CLLocationManager *locationManager;
    CLGeocoder *clGeocoder;
}
@property (retain, nonatomic) IBOutlet UILabel *CurrentLocLatt;
@property (retain, nonatomic) IBOutlet UILabel *CurrentLocLong;

@property (retain, nonatomic) IBOutlet UILabel *place;
@property (retain, nonatomic) IBOutlet UILabel *city;
@property (retain, nonatomic) IBOutlet UILabel *state;
@property (retain, nonatomic) IBOutlet UILabel *Country;


@end
