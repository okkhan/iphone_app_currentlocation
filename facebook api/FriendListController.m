//
//  FriendListController.m
//  5 Pillars of Islam
//
//  Created by Lazy Geeks on 9/17/12.
//
//

#import "FriendListController.h"
#import <QuartzCore/QuartzCore.h>
#import "FBRequestManager.h"
#import "Friend.h"
#import <QuartzCore/QuartzCore.h>

@interface FriendListController ()

@end

@implementation FriendListController
@synthesize tableViewFriends;
@synthesize friendSearchBar;
@synthesize challengeMessage;
@synthesize objectOfFriends;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        dataArray = [[NSMutableArray alloc] init];
        friendSearch = [[NSMutableArray alloc] init];
        challengeIdz = [[NSMutableArray alloc] init];
        
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(KeyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(KeyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    }
    return self;
}

-(void)KeyboardDidShow:(NSNotification *)notification{
    
    NSDictionary* userInfo = [notification userInfo];
    
    NSValue* keyboardFrameValue = [userInfo objectForKey:@"UIKeyboardBoundsUserInfoKey"];
    if (!keyboardFrameValue) {
        keyboardFrameValue = [userInfo objectForKey:@"UIKeyboardFrameEndUserInfoKey"];
    }
    
    CGRect tblViewFrame = self.tableViewFriends.frame;
    tblViewFrame.size.height-=[keyboardFrameValue CGRectValue].size.height;
    [self.tableViewFriends setFrame:tblViewFrame];
    
    
}
-(void)KeyboardDidHide:(NSNotification *)notification{
    
    NSDictionary* userInfo = [notification userInfo];
    
    NSValue* keyboardFrameValue = [userInfo objectForKey:@"UIKeyboardBoundsUserInfoKey"];
    if (!keyboardFrameValue) {
        keyboardFrameValue = [userInfo objectForKey:@"UIKeyboardFrameEndUserInfoKey"];
    }
    CGRect tblViewFrame = self.tableViewFriends.frame;
    tblViewFrame.size.height+=[keyboardFrameValue CGRectValue].size.height;
    [self.tableViewFriends setFrame:tblViewFrame];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //self.view.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]];
    
    self.tableViewFriends.backgroundColor = [UIColor clearColor];
    
    indexesOfSearchItems = [[NSMutableArray alloc] init];
    
    UIBarButtonItem *rightButton = [[[UIBarButtonItem alloc] initWithTitle:@"Send Challenge" style:UIBarButtonItemStylePlain target:self action:@selector(challengeFriend)]autorelease];
    [self.navigationItem setRightBarButtonItem:rightButton];
    
    searching = NO;
	letuserSelectRow = YES;
    
    friendSearchBar.delegate=self;
    
    [self loadData];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO];
}

-(void)challengeFriend
{
    FBRequestManager *manager = [FBRequestManager sharedinstance];
    
    NSLog(@"%@",challengeIdz);
    
    manager.challengIdz=challengeIdz;
    manager.challengeMessage=objectOfFriends.challengeMessage;
    
    [manager postOnFriendsWall];
    
    [self.navigationController popToRootViewControllerAnimated:YES];
    
}

- (void)viewDidUnload
{
    [self setTableViewFriends:nil];
    [self setFriendSearchBar:nil];
    [super viewDidUnload];
}

-(void)loadData
{
    FBRequestManager *mang = [FBRequestManager sharedinstance];
    
    mang.delegate=self;
    
    [mang getFriendsList];
    
}
-(UIImage *) makeRounded:(UIImage *)sourceImage 
{
	
	UIImage *image = sourceImage;
	
	UIImageView *myRoundedImageView = [[[UIImageView alloc] initWithImage:image]autorelease] ;
	
	CALayer *layer=[myRoundedImageView layer];
	
	[layer setMasksToBounds:YES];
	
	[layer setCornerRadius:5.0];
	
	UIGraphicsBeginImageContext(layer.bounds.size);
	[layer renderInContext:UIGraphicsGetCurrentContext()];
	UIImage *myImage=UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	return myImage;
}

#pragma mark - UITableViewDatasource and UITableViewDelegate Methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60.0;
}

// Customize the number of sections in the table view.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (searching)
		return [indexesOfSearchItems count];
    else
        return [dataArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier]autorelease];
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        [cell setNeedsDisplay];
    }
    Friend *aFriend;
    
    if (searching && indexesOfSearchItems.count > 0)
    {
        
        NSLog(@"%i",indexPath.row);
        NSLog(@"%@",indexesOfSearchItems);
        
        aFriend = [dataArray objectAtIndex:[[indexesOfSearchItems objectAtIndex:indexPath.row] integerValue]];
        
        [cell.textLabel setText:aFriend.friendName];
        [cell.detailTextLabel setText:aFriend.friendID];
    }
    else
    {
        aFriend = [dataArray objectAtIndex:indexPath.row];
        [cell.textLabel setText:aFriend.friendName];
        [cell.detailTextLabel setText:aFriend.friendID];
        
    }
    
    /*================Searchinhg==================*/
    
    
    BOOL checked = aFriend.isSelected;
    
    
    UIImage *image = (checked) ? [UIImage imageNamed:@"checked.png"] : [UIImage imageNamed:@"unchecked.png"];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    CGRect frame = CGRectMake(0.0, 0.0, image.size.width, image.size.height);
    button.frame = frame;	// match the button's size with the image size
    
    [button setBackgroundImage:image forState:UIControlStateNormal];
    
    // set the button's target to this table view controller so we can interpret touch events and map that to a NSIndexSet
    [button addTarget:self action:@selector(checkButtonTapped:event:) forControlEvents:UIControlEventTouchUpInside];
    button.backgroundColor = [UIColor clearColor];
    cell.accessoryView = button;
    
    
    /*============================================*/
    
    if (aFriend.friendImage != nil) {
        
        UIImage *image = [UIImage imageWithData:aFriend.friendImage];
        [cell.imageView setImage:[self makeRounded:image]];
        [cell setNeedsDisplay];
    }
    else {
        dispatch_queue_t concurrentQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        
        dispatch_async(concurrentQueue, ^{
            
            aFriend.friendImage = [NSData dataWithContentsOfURL:aFriend.friendImageURL];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if(self.view!=nil){
                    
                    UIImage *image = [UIImage imageWithData:aFriend.friendImage];
                    [cell.imageView setImage:[self makeRounded:image]];
                    [self.tableViewFriends reloadData];
                    
                }
                else{
                    dispatch_suspend(concurrentQueue);
                }
            });
        });
        
    }
    [cell setNeedsDisplay];
    
    return cell;
}

#pragma mark - FBSessionDelegate
- (void)fbDidLogout{
    NSLog(@"Logged Out");
}
- (void)fbDidLogin{
    NSLog(@"logged In");
}
- (void)fbDidNotLogin:(BOOL)cancelled{
    NSLog(@"cancelled logging in");
}
- (void)fbSessionInvalidated{
    NSLog(@"session invalidated");
}
- (void)fbDidExtendToken:(NSString*)accessToken
               expiresAt:(NSDate*)expiresAt{
    NSLog(@"token extended");
}

#pragma mark - FBRequestManagerDelegate
-(void)didFinishDownloadingFriends{
    
  FBRequestManager *man =  [FBRequestManager sharedinstance];
    dataArray = man.friendsArray;
    friendSearch=man.friendsSearch;
    [self.tableViewFriends reloadData];
}
-(void)didFailDownloadingFriends:(NSString *)errorMessage
{
    
}

/*==========================selection====================================*/
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  [self tableView: self.tableViewFriends accessoryButtonTappedForRowWithIndexPath: indexPath];
  [self.tableViewFriends deselectRowAtIndexPath:indexPath animated:YES];
    
}

- (void)checkButtonTapped:(id)sender event:(id)event
{
	NSSet *touches = [event allTouches];
	UITouch *touch = [touches anyObject];
	CGPoint currentTouchPosition = [touch locationInView:self.tableViewFriends];
	NSIndexPath *indexPath = [self.tableViewFriends indexPathForRowAtPoint: currentTouchPosition];
	if (indexPath != nil)
	{
		[self tableView: self.tableViewFriends accessoryButtonTappedForRowWithIndexPath: indexPath];
	}
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{	
    
    Friend *frienCheck;
    
    if (searching && indexesOfSearchItems.count > 0) 
    {
        frienCheck =[dataArray objectAtIndex:[[indexesOfSearchItems objectAtIndex:indexPath.row] integerValue]];
        
    }
    else 
    {
        frienCheck = [dataArray objectAtIndex:indexPath.row];
    }
    
    
    
    UITableViewCell *cell = [self.tableViewFriends cellForRowAtIndexPath:indexPath];    
    
    UIButton *button = (UIButton *)cell.accessoryView;
    
    BOOL checked;
    
    if (frienCheck.isSelected) 
    {
        checked = YES;
        
        frienCheck.isSelected = NO;
        
        if (searching && indexesOfSearchItems.count > 0) 
        {
            frienCheck =[dataArray objectAtIndex:[[indexesOfSearchItems objectAtIndex:indexPath.row] integerValue]];
            
            [challengeIdz removeObject:frienCheck.friendID];
        }
        else 
        {
            [dataArray replaceObjectAtIndex:indexPath.row withObject:frienCheck];
            
            [challengeIdz removeObject:frienCheck.friendID];
        }        
    }
    else 
    {
        checked = NO;
        
        frienCheck.isSelected = YES;
        
        
        if (searching && indexesOfSearchItems.count > 0) 
        {
            frienCheck =[dataArray objectAtIndex:[[indexesOfSearchItems objectAtIndex:indexPath.row] integerValue]];
            
            [challengeIdz addObject:frienCheck.friendID];
        }
        else 
        {
            [dataArray replaceObjectAtIndex:indexPath.row withObject:frienCheck];
            
            [challengeIdz addObject:frienCheck.friendID];
        }
    }
    
    UIImage *newImage = (checked) ? [UIImage imageNamed:@"unchecked.png"] : [UIImage imageNamed:@"checked.png"];
    [button setBackgroundImage:newImage forState:UIControlStateNormal];
    
    
    //    NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:indexPath.row inSection:0];
    //    NSArray* rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
    //    [self.tableView reloadRowsAtIndexPaths:rowsToReload withRowAnimation:YES];
    
    
}


/*======================searching=================================*/

- (void) searchBarTextDidBeginEditing:(UISearchBar *)theSearchBar {
	
	//This method is called again when the user clicks back from teh detail view.
	//So the overlay is displayed on the results, which is something we do not want to happen.
	if(searching)
		return;
	
	//Add the overlay view.
	if(ovController == nil)
		ovController = [[OverlayViewController alloc] initWithNibName:@"OverlayView" bundle:[NSBundle mainBundle]];
	
	CGFloat yaxis = self.navigationController.navigationBar.frame.size.height;
	CGFloat width = self.view.frame.size.width;
	CGFloat height = self.view.frame.size.height;
	
	//Parameters x = origion on x-axis, y = origon on y-axis.
	CGRect frame = CGRectMake(0, yaxis, width, height);
	ovController.view.frame = frame;
	ovController.view.backgroundColor = [UIColor grayColor];
	ovController.view.alpha = 0.5;
	
	ovController.rvController = self;
	
	[self.tableViewFriends insertSubview:ovController.view aboveSubview:self.parentViewController.view];
	
	searching = YES;
	 letuserSelectRow= NO;
	self.tableViewFriends.scrollEnabled = NO;
}

- (void)searchBar:(UISearchBar *)theSearchBar textDidChange:(NSString *)searchText {
    
	//Remove all objects first.
    [indexesOfSearchItems removeAllObjects];
	
	if([searchText length] > 0) {
		
		[ovController.view removeFromSuperview];
		searching = YES;
		letuserSelectRow = YES;
		self.tableViewFriends.scrollEnabled = YES;
		[self searchTableView];
	}
	else {
		
		[self.tableViewFriends insertSubview:ovController.view aboveSubview:self.parentViewController.view];
		
		searching = NO;
		letuserSelectRow = NO;
		self.tableViewFriends.scrollEnabled = NO;
	}
	
	[self.tableViewFriends reloadData];
}

- (void) searchBarSearchButtonClicked:(UISearchBar *)theSearchBar {
	
	[self searchTableView];
}

- (void) searchTableView {
	
	NSString *searchText = friendSearchBar.text;
	NSMutableArray *searchArray = [[[NSMutableArray alloc] init]autorelease];
	
	[searchArray addObjectsFromArray:friendSearch];
	
	for (NSString *sTemp in searchArray)
	{
		NSRange titleResultsRange = [sTemp rangeOfString:searchText options:NSCaseInsensitiveSearch];
		
		if (titleResultsRange.length > 0)
        {
            [indexesOfSearchItems addObject:[NSNumber numberWithInt:[searchArray indexOfObject:sTemp]]];
        }
	}
	
	searchArray = nil;
}

- (void) doneSearching_Clicked:(id)sender {
	
	friendSearchBar.text = @"";
	[friendSearchBar resignFirstResponder];
	
	letuserSelectRow = YES;
	searching = NO;
	self.tableViewFriends.scrollEnabled = YES;
	
	[ovController.view removeFromSuperview];
	[ovController release];
	ovController = nil;
	
	[self.tableViewFriends reloadData];
}

/*=======================================================*/

- (void)dealloc {
    [tableViewFriends release];
    [friendSearchBar release];
    [super dealloc];
}
@end
