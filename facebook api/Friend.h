//
//  Friend.h
//  FBTest
//
//  Created by seecs on 3/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Friend : NSObject
{
    NSString *friendName;
    NSString *friendID;
    
    NSURL *friendImageURL;
    NSData *friendImage;
    
    BOOL isSelected;
}
@property(nonatomic , strong) NSString *friendName;
@property(nonatomic , strong) NSString *friendID;
@property(nonatomic , strong) NSURL *friendImageURL;
@property(nonatomic , strong) NSData *friendImage;
@property(nonatomic ) BOOL isSelected;
@end
