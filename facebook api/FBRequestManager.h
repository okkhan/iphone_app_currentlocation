//
//  FBRequestManager.h
//  FBTest
//
//  Created by seecs on 3/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FBConnect.h"
#import "FBRequestManagerDelegate.h"
#import "StaticClass.h"

#define KFriendsRequest @"friends"

@interface FBRequestManager : NSObject <FBRequestDelegate>
{
    NSMutableArray *friendsArray;
    NSMutableArray *albumsArray;
    
    Facebook *facebookR;
    
    StaticClass *sharedObject;
}

@property(nonatomic, retain)id<FBRequestManagerDelegate>delegate;
@property(nonatomic , strong) NSMutableArray *friendsArray;
@property(nonatomic , strong) NSMutableArray *friendsSearch;

@property(nonatomic, strong) NSMutableArray *challengIdz;
@property(nonatomic,retain) NSString *challengeMessage;

@property(nonatomic) BOOL isSubmitScore;
@property(nonatomic) BOOL isShowFriends;
@property(nonatomic) BOOL isChallengeFriends;

@property(nonatomic , retain) NSString *message;


@property(nonatomic , strong) Facebook *facebookR;

@property (nonatomic, strong) UIImage *scoreImage;

+(FBRequestManager *)sharedinstance;
-(void)getFriendsList;
-(void)downloadFriendsFromServer:(id)result;

-(void)submitScore;

-(void)postOnFriendsWall;

//-(AppDelegate *)getAppDelegate;

@end
