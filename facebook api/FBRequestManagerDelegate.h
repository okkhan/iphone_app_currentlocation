//
//  FBRequestManagerDelegate.h
//  FBTest
//
//  Created by seecs on 3/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol FBRequestManagerDelegate <NSObject>

@optional

-(void)didFinishDownloadingFriends;
-(void)didFailDownloadingFriends:(NSString *)errorMessage;

-(void)didFinishDownloadingAlbums;
-(void)didFailDownloadingAlbums:(NSString *)errorMessage;

@end
