//
//  FriendListController.h
//  5 Pillars of Islam
//
//  Created by Lazy Geeks on 9/17/12.
//
//

#import <UIKit/UIKit.h>
#import "FBConnect.h"
#import "FBRequestManagerDelegate.h"
#import "OverlayViewController.h"

@interface FriendListController : UIViewController<FBDialogDelegate,
FBSessionDelegate,
UITableViewDataSource,
UITableViewDelegate ,
FBRequestManagerDelegate,UISearchBarDelegate>
{
    NSMutableArray *friendNames;
    NSMutableArray *friendzIdz;
    NSMutableArray *friendPict;
    
    NSMutableArray *dataArray;
    
    NSMutableArray *indexesOfSearchItems;
    
    NSMutableArray *friendSearch;
    
    NSMutableArray *challengeIdz;
    
    BOOL searching;
    BOOL letuserSelectRow;
    
    OverlayViewController *ovController;
}

-(void)loadData;
@property (retain, nonatomic) IBOutlet UITableView *tableViewFriends;
@property (retain, nonatomic) IBOutlet UISearchBar *friendSearchBar;

@property (retain , nonatomic)FriendListController *objectOfFriends;

@property (retain, nonatomic) NSString *challengeMessage;

-(UIImage *)makeRounded:(UIImage *)sourceImage;

-(void)challengeFriend;

- (void) searchTableView;
- (void) doneSearching_Clicked:(id)sender;

@end
