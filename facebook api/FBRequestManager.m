//
//  FBRequestManager.m
//  FBTest
//
//  Created by seecs on 3/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FBRequestManager.h"
#import "Friend.h"
#import "SBJSON.h"
#import "JSON.h"

#define KFacebookAppID @"422510464471940"

static FBRequestManager *sharedInstance = nil;

@implementation FBRequestManager
//@synthesize managerDelegate;
@synthesize delegate;
@synthesize friendsArray;
@synthesize facebookR;
@synthesize isSubmitScore;
@synthesize isShowFriends;
@synthesize message;
@synthesize friendsSearch;
@synthesize isChallengeFriends;
@synthesize challengeMessage;
@synthesize challengIdz;

@synthesize scoreImage;

-(id)init{
    self = [super init];
    if(self){
        friendsArray = [[NSMutableArray alloc] init];
        friendsSearch = [[NSMutableArray alloc] init];
        challengIdz=[[NSMutableArray alloc] init];
        self.isSubmitScore = NO;
        self.isShowFriends = NO;
        self.isChallengeFriends = NO;
    }
    return self;
}
+(FBRequestManager *)sharedinstance{
    if (sharedInstance == nil) {
		sharedInstance = [[[self class] alloc] init];
	}
	return sharedInstance;  
}

-(void)authorizeUser{
    
    if(facebookR == nil){
        facebookR = [[Facebook alloc] initWithAppId:KFacebookAppID andDelegate:self];
    }
    
    [facebookR authorize:[NSArray arrayWithObjects:@"publish_stream", nil]];
    
}

-(void)postOnFriendsWall
{
    self.isChallengeFriends= YES;
    if([facebookR isSessionValid])
    {
        
        NSMutableDictionary *params = [[[NSMutableDictionary alloc] init]autorelease];
        [params setValue:@"http://itunes.apple.com/us/app/islamic-pillars/id563853657?ls=1&mt=8" forKey:@"link"];
        [params setValue:challengeMessage forKey:@"message"];
        
        for (int i =0 ; i<challengIdz.count; i++) 
        {
            NSString *stringFr=[NSString stringWithFormat:@"%@/feed",[challengIdz objectAtIndex:i]];
            
            [facebookR requestWithGraphPath:stringFr andParams:params andHttpMethod:@"POST" andDelegate:self];
        }

    }
    else 
    {
        [self authorizeUser];
    }
}

-(void)submitScore
{
    sharedObject = [StaticClass sharedInstance];
    self.isSubmitScore=YES;
    if([facebookR isSessionValid])
    {
        
        NSMutableDictionary *params = [[[NSMutableDictionary alloc] init]autorelease];
        [params setValue:sharedObject.scoreImageFinal forKey:@"picture"];
        [params setValue:message forKey:@"message"];
       [facebookR requestWithGraphPath:@"photos" andParams:params andHttpMethod:@"POST" andDelegate:self];
    }
    else 
    {
        [self authorizeUser];
    }
}

-(void)getFriendsList
{
    self.isShowFriends=YES;
    if([facebookR isSessionValid])
    {
        
        NSMutableDictionary *params = [[[NSMutableDictionary alloc] init]autorelease];
        [params setValue:@"0" forKey:@"limit"];
        [params setValue:KFriendsRequest forKey:@"info"];
     [facebookR requestWithGraphPath:@"me/friends" andParams:params andDelegate:self];
    }
    else 
    {
        [self authorizeUser];
    }
}

-(void)downloadFriendsFromServer:(id)result{
    
    NSDictionary *jsonDictionary= nil;
    if([result isKindOfClass:[NSDictionary class]]){
        jsonDictionary = (NSDictionary *)result;
    }
    NSArray *array = [jsonDictionary objectForKey:@"data"];
    if(![array isKindOfClass:[NSNull class]]){
        [friendsArray removeAllObjects];
        for(NSDictionary *friendDict in array){
            Friend *aFriend = [[[Friend alloc] init]autorelease];
            
            NSString *friendID = [friendDict objectForKey:@"id"];
            aFriend.friendID = friendID;
            aFriend.friendName = [friendDict objectForKey:@"name"];
            aFriend.friendImage = nil;
            aFriend.friendImageURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture",friendID]];
            
            [friendsArray addObject:aFriend];    
            
            [friendsSearch addObject:[friendDict objectForKey:@"name"]];
        }
        [delegate didFinishDownloadingFriends];
    }
    else{
        [delegate didFailDownloadingFriends:@"No Friends Found"];
    }
    
}

#pragma mark - FBSessionDelegate

- (void)fbDidLogin{
    
   // [self getFriendsList];
    
    if (self.isShowFriends) 
    {
        [self getFriendsList];
    }
    
    if (self.isSubmitScore) 
    {
        [self submitScore];
    }
    
    if (self.isChallengeFriends) 
    {
        [self postOnFriendsWall]; 
    }
}

/**
 * Called when the user dismissed the dialog without logging in.
 */
- (void)fbDidNotLogin:(BOOL)cancelled
{
    
}
- (void)fbDidExtendToken:(NSString*)accessToken
               expiresAt:(NSDate*)expiresAt{
    
}
- (void)fbDidLogout{
    
    if(facebookR){
        [facebookR release];
        facebookR = nil;
    }
    
    UIAlertView *alert = [[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Logged Out Successfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease];
    [alert show];
    
}

- (void)fbSessionInvalidated{
    
}

#pragma mark - FBDialogDelegate
- (void)dialogDidComplete:(FBDialog *)dialog{
    
}

/**
 * Called when the dialog succeeds with a returning url.
 */
- (void)dialogCompleteWithUrl:(NSURL *)url{
    
}

/**
 * Called when the dialog get canceled by the user.
 */
- (void)dialogDidNotCompleteWithUrl:(NSURL *)url{
    
}

/**
 * Called when the dialog is cancelled and is about to be dismissed.
 */
- (void)dialogDidNotComplete:(FBDialog *)dialog{
    
}

/**
 * Called when dialog failed to load due to an error.
 */
- (void)dialog:(FBDialog*)dialog didFailWithError:(NSError *)error{
    
}

/**
 * Asks if a link touched by a user should be opened in an external browser.
 *
 * If a user touches a link, the default behavior is to open the link in the Safari browser,
 * which will cause your app to quit.  You may want to prevent this from happening, open the link
 * in your own internal browser, or perhaps warn the user that they are about to leave your app.
 * If so, implement this method on your delegate and return NO.  If you warn the user, you
 * should hold onto the URL and once you have received their acknowledgement open the URL yourself
 * using [[UIApplication sharedApplication] openURL:].
 */
- (BOOL)dialog:(FBDialog*)dialog shouldOpenURLInExternalBrowser:(NSURL *)url{
    return YES;
}

#pragma mark FBRequestDelegate
/**
 * Called just before the request is sent to the server.
 */
- (void)requestLoading:(FBRequest *)request{
    
}

/**
 * Called when the Facebook API request has returned a response.
 
 */
- (void)request:(FBRequest *)request didReceiveResponse:(NSURLResponse *)response{
    
}

/**
 * Called when an error prevents the request from completing successfully.
 */
- (void)request:(FBRequest *)request didFailWithError:(NSError *)error
{
    NSLog(@"%@",error); 
    
    if (self.isSubmitScore) 
    {
        UIAlertView *alert = [[[UIAlertView alloc] initWithTitle:@"Dublicate" message:@"Your score has already posted" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil] autorelease];
        [alert show];  
    }
}
/**
 * Called when a request returns and its response has been parsed into
 * an object.
 */
- (void)request:(FBRequest *)request didLoad:(id)result{
    NSDictionary *params = (NSDictionary *)[request params];
    
    // condition for friendsList
    if([[params valueForKey:@"info"] isEqualToString:KFriendsRequest]){
        
        [self downloadFriendsFromServer:result];
    }
    else if (self.isSubmitScore) 
    {
        UIAlertView *alert = [[[UIAlertView alloc] initWithTitle:@"Success" message:@"Your Score Has Been Shared On Facebook" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil] autorelease];
        [alert show];
    }
    else if (self.isChallengeFriends) 
    {
        UIAlertView *alert = [[[UIAlertView alloc] initWithTitle:@"Success" message:@"Your Challenge Has Been sent to selected friends" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil] autorelease];
        [alert show];
    }
}


/**
 * Called when a request returns a response.
 *
 * The result object is the raw response from the server of type NSData
 */
- (void)request:(FBRequest *)request didLoadRawResponse:(NSData *)data{
    
}
@end
